# puts the number 100 into the variable cars
cars = 100

# Gives the floating point number 4.0 to the variable space_in_a_car
space_in_a_car = 4.0

# gives the variable drivers the number 30
drivers = 30

# gives the passengers variable the number 90
passengers = 90

# creates a variable with math using variables already assigned. 100 - 30
cars_not_driven = cars - drivers

#gives the variable cars_driven the string drivers
cars_driven = drivers

# another variable containing math using already assigned variables
carpool_capacity = cars_driven * space_in_a_car

# and another variable containing already assigned variable math
average_passengers_per_car = passengers / cars_driven

puts "There are #{cars} cars available."
puts "There are only #{drivers} drivers available."
puts "There will be #{cars_not_driven} empty cars today."
puts "We can transport #{carpool_capacity} people today."
puts "We have #{passengers} to carpool today."
puts "We need to put about #{average_passengers_per_car} in each car."

