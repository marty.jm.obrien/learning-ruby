def translate(input)
    to_translate = input.split ' '
    to_translate.each do |w|
        first_letter = w[0]
        first_two_letters = w[0..1]
        first_three_letters = w[0..2]
        second_third_letters = w[1..2]
        if first_three_letters !~ (/[AEIOUaeiou]/)
            w.delete!(first_three_letters)
            w.replace(w + first_three_letters + "ay")
        elsif second_third_letters == "qu" && w[0] !~ (/[AEIOUaeiou]/)
            w.delete!(first_three_letters)
            w.replace(w + first_three_letters + "ay")
        elsif first_two_letters !~ (/[AEIOUaeiou]/)
            w.delete!(first_two_letters)
            w.replace(w + first_two_letters + "ay")
        elsif w[0] !~ (/[AEIOUaeiou]/) && w[1] == "u"
            w.delete!(first_two_letters)
            w.replace(w + first_two_letters + "ay")
        elsif w[0] !~ (/[AEIOUaeiou]/)
            w.delete!(w[0])
            w.replace(w + first_letter + "ay")
        else
            w.replace(w + "ay")
        end
    end
    if to_translate.length >= 2
        to_translate.join ' '
    else
        to_translate[0]
    end
end
