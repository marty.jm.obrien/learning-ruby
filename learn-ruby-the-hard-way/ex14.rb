first, second = ARGV
prompt = "..."

puts "Hi #{first}."
puts "I'd like to ask you a few questions."
puts "Do you like me #{first}?", prompt
likes = $stdin.gets.chomp

puts "Where do you live #{first}?", prompt
lives = $stdin.gets.chomp

puts "What kind of computer do you have?", prompt
computer = $stdin.gets.chomp

puts "Is #{second} really your favorite?", prompt
color = $stdin.gets.chomp

puts """
Alright, so you said #{likes} about liking me.
You live in #{lives}. Not sure where that is.
You you have a #{computer} computer. Nice.
And your confirmation of #{second} being your favorite is: #{color}.
"""