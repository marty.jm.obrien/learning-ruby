# def while_loop(i, max, increment)
# 
# numbers = []
# 
# while i < max
#   puts "At the top i is #{i}"
#   numbers.push(i)
#   
#   i += increment
#   puts "Numbers now: ", numbers
#   puts "At the bottom i is #{i}"
# end
# 
# puts "The numbers: "
# 
# numbers.each {|num| puts num }
# 
# end
# 
# while_loop(0, 6, 1)


def while_loop(i, max, increment)

numbers = []

(i...max).each do |i|
  puts "At the top i is #{i}"
  numbers.push(i)
  
  i += increment
  puts "Numbers now: ", numbers
  puts "At the bottom i is #{i}"
end

puts "The numbers: "

numbers.each {|num| puts num }

end

while_loop(0, 6, 1)