## Animal is-a object look at the extra credit
class Animal
end

## Class Dog is a Animal
class Dog < Animal

  def initialize(name)
    ## Dog has a name of some kind
    @name = name
  end
end

## Class Cat is a Animal
class Cat < Animal

  def initialize(name)
    ## Cat has a name of some kind
    @name = name
  end
end

## class Person is a object
class Person

  def initialize(name)
    ## Person has a name of some kind
    @name = name

    ## Person has-a pet of some kind
    @pet = nil
  end

  attr_accessor :pet
end

## class employee is a person
class Employee < Person

  def initialize(name, salary)
    ## ?? hmm what is this strange magic?
    super(name)
    ## employee has a salary
    @salary = salary
  end

end

## fish is a object
class Fish
end

## class salmon is a fish
class Salmon < Fish
end

## class halibut is a fish
class Halibut < Fish
end


## rover is-a Dog
rover = Dog.new("Rover")

## satan is a cat
satan = Cat.new("Satan")

## mary is a person
mary = Person.new("Mary")

## mary has a pet named satan
mary.pet = satan

## frank is a employee with a salary of 120000
frank = Employee.new("Frank", 120000)

## frank has a pet named rover
frank.pet = rover

## flipper is a fish
flipper = Fish.new()

## crouse is a salmon
crouse = Salmon.new()

## harry is a halibut
harry = Halibut.new()