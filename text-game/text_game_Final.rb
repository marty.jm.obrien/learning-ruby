class Player
  attr_accessor :name, :code, :correct_code
  
    def initialize
      @name = "unknown"
      @code = 2851
      @correct_code = 5812
    end
    
    def show_code
      @code
    end
    
    def correct_code
      @correct_code
    end
end 

def get_player_name(player)
  puts "What is your name intrepid explorer?"
  player_name = gets.chomp
  @name = player_name
  puts """
  Welcome #{player_name}. Please type commands in a natural manner to explore your
  surroundings or input actions. At the \"> \" prompt please input what you would
  like to do.
  """
end

def start_game(player)
  puts "Shall we begin #{@name}? Yes or No?"
  print "> "
  start_game = gets.chomp
  start_game.downcase!
  
  if start_game == "yes"
    puts "Let the adventure begin!"
  elsif start_game == "no"
    puts "Come back when your courage is stronger!"
    exit
  else
    puts "Sorry, I didn't recognise that."
    start_game(p)
  end
end

def game_intro(player)
  puts "------------------------------"
  puts "Entrance Hall"
  puts "------------------------------"
  puts """
  You land heavily onto the stone floor.
  As you look back the heavy iron doors behind you close with a thud.
  Looking around you can make out two doors in the low light.
  As you scan the room a large neon sign buzzes on above one of the doors, flickering until steady.
  Looking up you read the words:
  
  BEHIND THIS DOOR FOUR NUMBERS CAN BE FOUND.
  
  Suddenly a second sign buzzes into life above the second door:
  
  BEHIND THIS DOOR FREEDOM CAN BE FOUND.
  
  Looking down you see a four digit coded padlock across a large bolt.
  You turn back towards the first door and hear it unlock.
  """
  
  user_choice = false
  
  until user_choice == true
    print "> "
    user_input = gets.chomp
    
    if user_input.include?("open") || user_input.include?("enter")
      puts """
      You turn the handle and the door opens with a low rumbling groan.
      """
      user_choice = true
    else
      puts """
      There is little else than dust and stone in the entrance hall.
      """
    end
  end 
end

def door_one_story(player)
  puts "------------------------------"
  puts "Door One"
  puts "------------------------------"
  
  puts """
  As you walk through the door you are confronted by a cold concrete room.
  You turn to get out only to find the door now locked.
  In the center of the room there is a carved stone church font, used to baptise children.
  Next to the font there is a small wooden chair.
  Hanging from the ceiling there is a single light bulb.
  On the wall there is a large painting.
  As your eyes scan the room someone suddenly starts to speak.\n
  \"Welcome #{@name}. I\'ve been told this puzzle can sting, but don't worry, 
  I\'ve also heard that losing is far more painful. As you can see there are 4 types of
  object in the room and there are four numbers to be found. Have a look around and see
  if you can find them #{@name}. I will be watching...\" 
  """
end

def door_one_choices(player)
  puts "What would you like to inspect?"
  
  user_choice = false
  
  until user_choice == true
    print"> "
    user_choice = gets.chomp
    user_choice.downcase!
  
    if user_choice.include?("room")
      puts """
      You glance around the room but nothing other than the 4 objects catches your eye.
      """
    elsif user_choice.include?("chair")
      puts """
      You walk over to the chair and take note of its sturdiness.
      You try to move the chair but find it stuck to the concrete floor.
      Above it is the central light bulb.
      """
    elsif user_choice.include?("light") || user_choice.include?("bulb")
      puts """
      The light bulb is hanging directly over the chair and appears to be screw in....
      """
    elsif user_choice.include?("painting")
      puts """
      The painting is of Jesus crying but his tears have been coloured to look like blood.
      """
    elsif user_choice.include?("font")
      puts """
      You walk up to the font and touch the cool stone.
      You take in it's smoothness and the intricate decorations around it's edge.
      There is an inscription carved in and around the decorations.
      """
      user_choice == true
      break
    else
      puts "That isn\'t inside the room or contains no clues."
    end
  end
end

def font(player)
  puts "Read the inscription or look into the water?"
  
  user_choice = false
  
  until user_choice == true
    print"> "
    user_choice = gets.chomp
    user_choice.downcase!
    
    if user_choice.include?("read") || user_choice.include?("inscription")
      puts """
      Although faded you can just make out the words:
      Water is clear, yet the sea is blue, what colour will I be, when I get something from you?\n
      You look into the clear water and see your reflection as you try to understand the inscription.
      As you do you see writing on the bottom:
      It is blue like the sea but red in the air. It can be spilt like water with a cut, if you dare.
      You look around for something to make a cut.
      """
      user_choice = true
      break
    elsif user_choice.include?("water")
      puts """
      Only your reflection stares back at you.
      """
    else
      puts """
      You can't do that.
      """
    end
  end  
end

def bulb(player)
  puts "What do you use?"
  
  user_choice = false
  
  until user_choice == true
    print"> "
    user_choice = gets.chomp
    user_choice.downcase!
    
    if user_choice.include?("light") || user_choice.include?("bulb")
        puts """
        You stand on the chair and unscrew the light bulb. As you step back down you feel
        the smoothness of the glass and wonder what to do you it.
        """
        user_choice = true
        break
    elsif cut_tool.include?("chair")
        puts """
        The chair cannot be moved and cannot be broken.
        """
    elsif cut_tool.include?("font")
        puts """
        The smooth stone cannot cut.
        """
    else
        puts """
        Please choose again.
        """
    end
  end
end

def cut(player)
  
  user_choice = false
  
  until user_choice == true
    print"> "
    user_choice = gets.chomp
    user_choice.downcase!
      
    if user_choice.include?("smash") || user_choice.include?("break")
      puts """
      You break the light bulb on the edge of the font.
      Picking up one of the shards you cut your finger and watch a droplet of blood form.
      As the blood drips into the water within the font it turns a deep, rich red.
      You hear the click and rough slide of a stone mechanism followed by a low gargle.
      The bloody water rushes out of the font.
      As the last drops drain away 4 numbers appears on the bottom of the font bowl and the room
      is suddenly filled with pre-recorded TV applause.
      You run for the door, reapeating the numbers you have just found. You reach for the handle
      and find that this time it turns, the door has been unlocked. You run into the entrance
      hall once more and immediately hear the door lock behind you.
      """
      user_choice == true
      break
    else
      puts """
      Doing that will not help you.
      """
    end
  end
end

def door_two_story(player)
  puts """
  Walking up to the padlocked door, you recite the numbers you just saw\n
  \"#{player.show_code}, #{player.show_code}, #{player.show_code}....\"\n
  You reach for the padlock and twist the digits until the numbers match, 2851, and then push the
  unlock button....\n
  To your surprise and disappointment the code doesn't work. Maybe it's a different combination?
  You look up and notice that the neon sign above the door has now changed.\n
  3 tries to get it right. Here is a hint. The year we all had Vertigo with the year Django was unchained.
  """
  
  code_chances = 0
  user_input = false
  
  until user_input == true
  print "> "
  user_choice = gets.chomp.to_i
  
    if user_choice == player.correct_code
      puts """
      You put in the new combination and the padlock opens! You open the door and run to freedom.
      """
      user_input = true
    elsif
      code_chances == 0
      code_chances += 1
      puts """
      That combination doesn't work. You look up and the sign now says 2 tries remaining.
      """
    elsif
      code_chances == 1
      code_chances += 1
      puts """
      That combination doesn't work. You look up and the sign now says 1 tries remaining.
      You gather your nerves for the last try.
      """
    else
      code_chances == 2
      code_chances += 1
      puts """
      You put the code in a try the padlock. Nothing happens and you see the remaining 
      tries counter change to a zero. You take a step back, away from the door, and as you do so
      you feel a presense behind you. As you turn to see what is there your world turns black.
      You are dead.
      """
      exit
    end
  end
end
    
p = Player.new

get_player_name(p)
start_game(p)
game_intro(p)
door_one_story(p)
door_one_choices(p)
font(p)
bulb(p)
cut(p)
door_two_story(p)