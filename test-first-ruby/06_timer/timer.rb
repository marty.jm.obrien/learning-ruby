class Timer
  def initialize
    @seconds = 0
    @time_string = "00:00:00"
  end
  def seconds
    @seconds
  end
  def seconds=(new_seconds)
    @seconds = new_seconds
  end
  def time_string
    if @seconds < 60
      if @seconds < 10
        @time_string = "00:00:0#{@seconds}"
      else
        @time_string = "00:00:#{@seconds}"
      end
    elsif @seconds >= 60 && @seconds < 3600
      time_with_float = @seconds / 60.00
      minutes = time_with_float.floor
      remainder_seconds = (time_with_float.modulo(1).round(2)) * 60
        if remainder_seconds.modulo(1) < 0.5
          remainder_seconds = remainder_seconds.floor
        else
          remainder_seconds = remainder_seconds.ceil
        end
        if minutes < 10 && remainder_seconds < 10
          @time_string = "00:0#{minutes}:0#{remainder_seconds}"
        else
          @time_string = "00:#{minutes}:#{remainder_seconds}"
        end
    elsif @seconds >= 3600
      time_with_float = @seconds / 60.00
      hours = (time_with_float / 60).floor
      minutes = (((time_with_float / 60).modulo(1).round(2)) * 60).floor
      remainder_seconds = (time_with_float.modulo(1).round(2)) * 60
        if remainder_seconds.modulo(1) < 0.5
          remainder_seconds = remainder_seconds.floor
        else
          remainder_seconds = remainder_seconds.ceil
        end
        if hours < 10 && minutes < 10 && remainder_seconds < 10
          @time_string = "0#{hours}:0#{minutes}:0#{remainder_seconds}"
        elsif hours < 10 && minutes < 10 && remainder_seconds >= 10
          @time_string = "0#{hours}:0#{minutes}:#{remainder_seconds}"
        elsif hours < 10 && minutes >= 10 && remainder_seconds >= 10
          @time_string = "0#{hours}:#{minutes}:#{remainder_seconds}"
        elsif hours >= 10 && minutes >= 10 && remainder_seconds >= 10
          @time_string = "0#{hours}:0#{minutes}:#{remainder_seconds}"
        end
    end
  end
end