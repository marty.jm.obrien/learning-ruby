def add(num1, num2)
    num1 + num2
end

def subtract(num1, num2)
    num1 - num2
end

def sum(array)
    if array.empty?
        return 0
    else
        array.reduce(:+)
    end
end

def multiply(*numbers)
    numbers.reduce(:*)
end

def power(num, power)
    num ** power
end

# def factorial(num)
#     if num == 0 || num == 1
#         1
#     else
#         factorial = num
#         factorial_sum = 0
#         until factorial == 1
#             if factorial_sum == 0
#                 factorial_sum += factorial * (factorial - 1)
#                 factorial -= 1
#             else
#                 factorial_sum = factorial_sum * (factorial - 1)
#                 factorial -= 1
#             end
#         end
#         return factorial_sum
#     end
# end

# def factorial(num)
#     if num == 0 || num == 1
#         1
#     else
#     factorial_array = []
#     num.downto(1) do |n|
#         factorial_array << n
#     end
#     factorial_array.reduce(:*)
#     end
# end

def factorial(num)
    if num == 0 || num == 1
        1
    else
    num.downto(1).reduce(:*)
    end
end