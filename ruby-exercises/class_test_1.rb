class UserOne
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def welcome
    puts "Hello #{name}"
  end
end

puts "Please tell me your name"
print "> "
user_name = gets.chomp

user = UserOne.new(user_name)
user.welcome
