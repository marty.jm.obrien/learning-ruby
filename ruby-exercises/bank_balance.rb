# Code written by Marty O'Brien, June 2016

pin = 3456
funds = 100

print "Hi, please input your username: "
user_name = gets.chomp

puts "Welcome #{user_name}. "

print "Please enter your pin number to view your current balance and withdraw money: "
user_pin = gets.chomp.to_i

if user_pin == pin
	puts "Your current balance is £#{funds}."
	puts "How much would you like to withdraw?"
	withdrawel = gets.chomp.to_i
else
	puts "Sorry, that pin is incorrect."
end

remaining_funds = funds - withdrawel

puts "Thank you for banking with us today. Your new account balance is £#{remaining_funds}."
