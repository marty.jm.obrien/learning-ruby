lottery_numbers = [1,3,6,11,66,87,65]
user_numbers = []
winning_numbers = []

puts "Please choose your lottery numbers"

until user_numbers.length == 7
  print "> "
  user_input = gets.chomp.to_i
  user_numbers.push(user_input)
end

user_numbers.each do |n|
  if lottery_numbers.include?(n)
     winning_numbers.push(n) unless winning_numbers.include?(n)
  end
end

if winning_numbers.length == 7
  puts "Congratulations! You have won the jackpot!"
elsif winning_numbers.length >= 3
  puts "Well done, you have won a prize!"
else
  puts "Sorry, better luck next time!"
end
