def abbreviate(string)
  input_string = string.split(" ")
  abb_string = []

  input_string.each do |i|
    if i.length >= 4
      a = i.length - 2
      i[1...-1] = a.to_s
      abb_string << i
    else
      abb_string << i
    end
  end

  puts abb_string.join(" ")
end

abbreviate("banana tree top to monkey so high")
