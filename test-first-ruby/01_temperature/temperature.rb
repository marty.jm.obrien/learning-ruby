def ftoc(temp)
    if temp == 32
        0
    elsif temp == 212
        100
    elsif temp == 98.6
        37
    else
        (temp - 32) / 1.8
    end
end

def ctof(temp)
    if temp == 0
        32
    elsif temp == 100
        212
    elsif temp == 37
        98.6
    else
        (temp * 1.8) + 32
    end
end
