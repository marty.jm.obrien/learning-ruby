# fruit = ['apple', 'orange', 'banana', 'grapefruit', 'kiwi']

# puts fruit[0]
# fruit.pop
# puts fruit
# fruit.push('mango')
# puts fruit
# fruit.delete('apple')
# puts fruit
# fruit << 'hello'
# puts fruit

# ranked_fruit = {
# 1 => 'apple',
# 2 => 'orange',
# 3 => 'banana',
# 4 => 'grapefruit',
# 5 => 'kiwi'
# }
# 
# puts ranked_fruit
# 
# puts "My fave fruit is #{ranked_fruit[1]}"
# 
# ranked_fruit[6] = "mango"
# 
# puts ranked_fruit
# 
# ranked_fruit[1] = "pear"
# 
# puts ranked_fruit



# fave_fruit = Hash.new(0)

# class FavoriteFruits
#   
#   attr_accessor :fave_fruit
# 
#   def initialize
#     @fave_fruit = Hash.new(0)
#   end
#   
#   def add_fruit(rank, fruit)
#     fave_fruit[rank] = fruit
#   end
#   
#   def print_fruit
#     puts "Here are your favorite fruits ranked:"
#     fave_fruit.each {|rank, fruit| puts "#{rank}: #{fruit}"}
#   end
#   
# end
#     
# ranked_fruits = FavoriteFruits.new
# 
# puts "Let's rank your favorite fruits\n"
# 
# puts "Please tell me your number one favorite fruit"
# print "> "
# fruit_one = gets.chomp
# ranked_fruits.add_fruit(1, fruit_one)
# 
# puts "Now your second favorite fruit"
# print "> "
# fruit_two = gets.chomp
# ranked_fruits.add_fruit(2, fruit_two)
# 
# puts "And finally your third favorite fruit"
# print "> "
# fruit_three = gets.chomp
# ranked_fruits.add_fruit(3, fruit_three)
# 
# ranked_fruits.print_fruit


class UserSignup
  attr_accessor :user_details
  
  def initialize
    @user_details = Hash.new(0)
  end
  
  def add_user_details(question, response)
    user_details[question] = response
  end
  
  def add_user_password(password)
    user_password(password)
  end
  
  def user_login
    login_email = nil
    login_password = nil
    until login_email == user_details["Email"] && login_password == user_password
      puts "Please enter your email address"
      print "Email: "
      login_email = gets.chomp
      puts "Please enter your password"
      print "Password: "
      login_password = gets.chomp
    end
    
      if login_email == user_details["Email"] && login_password == user_password
        puts "Welcome #{user_details["Name"]}"
      else
        return login_correct?
      end
  end
  
  private
  
    def user_password(password)
      user_password = password
    end

end

user = UserSignup.new

puts "To sign up, simple enter the following details"
puts "Name"
print "> "
user_name = gets.chomp
user.add_user_details("Name", user_name)
puts "Email"
print "> "
user_email = gets.chomp
user.add_user_details("Email", user_email)
puts "Password"
print "> "
password = gets.chomp
user.add_user_password(password)

# puts user.show_user_details
puts "Thank you for signing up #{user.user_details["Name"]}!"
puts "Now please login to your new account"

user.user_login