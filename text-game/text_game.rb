def code(num) 
    puts "Well done. You have found an escape code digit! The digit is #{num}."
      $padlock_code.push(num)
        if $padlock_code.length == 4
          puts "You now have found all four digits!"
          puts "The digits are #{$padlock_code}."
          end_room
        elsif $padlock_code.length >= 2
          puts "So far you have found #{$padlock_code.length} digits."
          entrance_again
        else
          puts "So far you have found #{$padlock_code.length} digit."
          entrance_again
        end
end

def entrance_again
  puts "You exit door one and it slams behind you."
  puts "Once again you are met with the doors at the entrance."
  puts "This time you decide to go through door number:"
  print "> "

  first_door_choice = $stdin.gets.chomp
  first_door_choice.downcase!
  first_door_choice.to_i

  if first_door_choice == "1" || first_door_choice == "one"
    puts "You walk towards door number one and open it."
    door_one
      elsif first_door_choice == "2" || first_door_choice == "two"
    puts "You walk towards door number two and open it."
    door_two
      elsif first_door_choice == "3" || first_door_choice == "three"
    puts "You walk towards door number three and open it."
      door_three
    elsif first_door_choice == "4" || first_door_choice == "four"
      puts "You walk towards door number four and open it."
    door_four
    else
      puts "Please choose again."
      entrance_door_choice
  end
end

def entrance_door_choice
  puts "Nervously you look at each door, not knowing what lies behind."
  puts "With no way out you decide to go through door number:"
  print "> "

  first_door_choice = $stdin.gets.chomp
  first_door_choice.downcase!
  first_door_choice.to_i

  if first_door_choice == "1" || first_door_choice == "one"
    puts "You walk towards door number one and open it."
    door_one
      elsif first_door_choice == "2" || first_door_choice == "two"
    puts "You walk towards door number two and open it."
    door_two
      elsif first_door_choice == "3" || first_door_choice == "three"
    puts "You walk towards door number three and open it."
      door_three
    elsif first_door_choice == "4" || first_door_choice == "four"
      puts "You walk towards door number four and open it."
    door_four
    else
      puts "Please choose again."
      entrance_door_choice
  end
end

def door_one
  puts "------------------------------"
  puts "Door One"
  puts "------------------------------"
  
  puts """
  As you walk through door one you are confronted by a cold concrete room.
  In the center of the room there is a carved stone church font, used to baptise children.
  Next to the font there is a small wooden chair.
  Hanging from the ceiling, evenly spaced, are five light bulbs illuminating the room. 
  """
  door_one_choice
end

def door_one_choice  
  puts   "What do you do next?"
  print "> "
  
  user_choice = $stdin.gets.chomp
  user_choice.downcase!
  
  if user_choice.include?("room")
    puts "You look around but find nothing else of interest or use."
    door_one_choice
  elsif user_choice.include?("chair")
    puts """
    You walk over to the chair and take note of its sturdiness.
    You try to move the chair but find it stuck to the concrete floor.
    Above it is the central light bulb.
    """
    door_one_choice
  elsif user_choice.include?("light") || user_choice.include?("bulbs")
    puts """
    The light bulbs are all hanging in a pattern around the room.
    The central bulb is hanging directly over the chair and appears to be screw in.
    """
    door_one_choice
  elsif user_choice.include?("font")
    puts """
    You walk up to the font and touch the cool stone.
    You take in it's smoothness and the intricate decorations around it's edge.
    There is an inscription carved in and around the decorations.
    """
    font
  else
    puts "That isn\'t inside the room."
    door_one_choice
  end
end

def font
  puts "Read the inscription or look into the water?"
  print "> "
  
  user_choice = $stdin.gets.chomp
  user_choice.downcase!
    
  if user_choice.include?("read") || user_choice.include?("inscription")
    puts """
    Although faded you can just make out the words:
    Water is clear, yet the sea is blue, what colour will I be, when I get something from you?\n
    You look into the clear water and see your reflection as you try to understand the inscription.
    As you do you see writing on the bottom:
    It is blue like the sea but red in the air. It can be spilt like water with a cut, if you dare.
    You look around for something to make a cut.
    """
    cut
  elsif user_choice.include?("water") || user_choice.include?("look")
    puts "Only your reflection stares back at you."
    font
  else
    puts "You can't do that."
    font
  end
end

def cut
  puts "What do you use?"
  print "> "
  cut_tool = $stdin.gets.chomp
  cut_tool.downcase!
  
    if cut_tool.include?("light")
      puts """
      You stand on the chair and unscrew the light bulb.
      Glass can cut but not while smooth. What do you do?
      """
      print "> "
      bulb_choice = $stdin.gets.chomp
      
        if bulb_choice.include?("smash") || bulb_choice.include?("break")
          puts """
          You break the light bulb on the edge of the font.
          Picking up one of the shards you cut your finger and watch a droplet of blood form.
          As the blood drips into the water within the font it turns a deep, rich red.
          You hear the click and rough slide of a stone mechanism followed by a low gargle.
          The bloody water rushes out of the font leaving only a single digit on the bottom of the bowl.
          """
          code("3")
        else
          puts "That action isn't useful."
          cut
        end
    elsif cut_tool.include?("chair")
      puts "The chair cannot be moved and cannot be broken."
      cut
    elsif cut_tool.include?("font")
      puts "The smooth stone cannot cut."
      cut
    else
      puts "That choice is not within the room."
      cut
    end
end
 
def end_room
   puts "Well done, you have made it to the final challenge!"
end

$padlock_code = []

puts "What is your name intrepid explorer?"
print "> "
user_name = $stdin.gets.chomp
puts "Welcome #{user_name}, let the adventure begin!"

puts "------------------------------"
puts "Entrance Hall"
puts "------------------------------"

puts """
The heavy iron doors close behind you with a thud.
Looking around you can make out four doors in the low light.
As you scan the room a large neon sign buzzes on, flickering until steady.
Looking up you read the words:\n
CHOOSE A DOOR AND FIND THE CODE. FOUR DOORS, FOUR DIGITS\n
"""
entrance_door_choice