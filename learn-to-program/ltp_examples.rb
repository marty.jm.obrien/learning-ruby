# Example 1
# puts 24 * 365
# puts 10 * (365 * (60 * 24))
# puts 30 * (365 * (24 * (60 * 60)))
# # puts 1025000000
# puts 1025000000 / 60 / 60 / 24 / 365

# Example 5.6 1
# puts "Please tell me you first name"
# first_name = gets.chomp
# puts "Now please tell me your middle name(s)"
# middle_name = gets.chomp
# puts "And finally your last name"
# last_name = gets.chomp
# puts "Welcome #{first_name} #{middle_name} #{last_name}."

# Example 5.6 2
# puts "What is your favorite number?"
# favorite_number = gets.chomp.to_i
# favorite_number += 1
# puts "I think #{favorite_number} is much better!"

# puts self

# Example 6.1
# name = "Marty"
# age = 30
# puts name.reverse
# puts age.to_s.reverse
# puts name.length
# puts age.to_s.length
# puts "#{name}, you are #{age}."

# puts "Please tell me you first name"
# first_name = gets.chomp
# puts "Now please tell me your middle name(s)"
# middle_name = gets.chomp
# puts "And finally your last name"
# last_name = gets.chomp
# puts "Welcome #{first_name} #{middle_name} #{last_name}."
# puts "Your name has #{first_name.length + middle_name.length + last_name.length} characters."

# Example 6.2
# puts "Hello employee, how can I help you today?"
# response = gets.chomp
# puts "What the hell do you mean \"#{response}\"?! You\'re fired!"

# line_width = 50
# puts "Table of Contents".center(line_width)
# puts ' '
# puts "Chapter 1:  Getting Started".ljust(line_width) + "page  1".rjust(line_width/5)
# puts "Chapter 2:  Numbers".ljust(line_width) + "page  9".rjust(line_width/5)
# puts "Chapter 3:  Letters".ljust(line_width) + "page 13".rjust(line_width/5)

# Example 7.2
# puts "How many lives does a cat have?"
# answer = gets.chomp.to_i
#
# if answer == 9
#   puts "Correct!"
# else
#   puts "Really? Have another go"
#   answer_two = gets.chomp.to_i
#     if answer_two == 9
#       puts "There we go!"
#     else
#       puts "Ugh, you\'re an idiot!"
#     end
# end

# Example 7.3 1
# input = ''
#
# while input != 'bye'
#   puts input
#   input = gets.chomp
# end
#
# puts 'Come again soon!'

# Example 7.3 2

# puts "Guess how many fingers I am holding up"
# print "> "
#
# while true
#   user_guess = gets.chomp.to_i
#     if user_guess == 6
#       break
#     else
#       puts "Wrong! Try again!"
#     end
# end
#
# puts "That\'s correct!"

# puts "Welcome to my random number generator!"
# puts "You give me a number up to 10 digits and I give you a random one back!"
# puts "To exit, just type exit"
# puts ''
# puts "Please choose your first number...."
# print "> "
#
# breaker = ''
#
# while true
#   user_num = gets.chomp
#
#   if user_num.length <= 10
#     puts "Random number: #{rand(user_num.to_i..10000000000)}"
#     breaker
#     puts "Please choose another"
#     print "> "
#   elsif user_num.length > 10
#     puts "Please only choose numbers with 10 or less digits"
#     print "> "
#   else
#     break
#   end
# end
#
# puts "Thanks for playing!"

# puts "Please choose a minimum number"
# min_num = gets.chomp.to_i
# puts "Now choose a maximum number"
# max_num = gets.chomp.to_i
#
# random_num = rand(min_num..max_num)
#
# puts "Here is your random number..."
# puts random_num

# Example 7.5

# bottles = 99
#
# while bottles >= 1
#   puts "#{bottles} of beer on the wall, #{bottles} of beer"
#   bottles -= 1
#   puts "Take one down and pass it around, #{bottles} of beer on the wall"
# end

# bye = 0
# while true
#   puts "What would you like to ask grandma?"
#   question = gets.chomp
#   if question == question.upcase && question != 'BYE'
#     puts "NO, NOT SINCE 19#{rand(38...50)}!"
#     bye = 0
#   elsif question == 'BYE' && bye == 2
#     puts "NICE TALKIN\' SONNY!"
#     exit
#   elsif question == 'BYE'
#     bye += 1
#     puts "HUH?! SPEAK UP, SONNY!"
#   else
#     puts "HUH?! SPEAK UP, SONNY!"
#     bye = 0
#   end
# end

# Worked out with help. Original was close but didn't know 'next if'!
# puts "Please enter a start year"
# print "> "
# start_year = gets.chomp.to_i
# puts "And now please enter an end year"
# print "> "
# end_year = gets.chomp.to_i
#
# (start_year..end_year).each do |year|
#   next if year % 4 != 0
#   next if year % 100 == 0 && year % 400 != 0
#   puts year
# end

# Exercise 8.3
# puts "Please enter as many words as you like, each on a new line."
# puts "When done, simply hit enter on a blank line."
# puts "Please enter your first word to start."
#
# words = []
#
# while true
#   new_word = gets.chomp
#   if new_word.empty?
#     break
#   else
#     puts "Your word: #{new_word}"
#     words.push(new_word)
#   end
# end
#
# puts "Here are your words, sorted alphabetically: #{words.sort.join(', ')}"

# table_contents = ["Table of Contents", "Chapter 1:  Getting Started", "Chapter 2:  Numbers", "Chapter 3:  Letters", "page  1", "page  9", "page 13"]
# line_width = 50
# puts table_contents[0].center(line_width)
# puts ' '
# puts table_contents[1].ljust(line_width) + table_contents[4].rjust(line_width/5)
# puts table_contents[2].ljust(line_width) + table_contents[5].rjust(line_width/5)
# puts table_contents[3].ljust(line_width) + table_contents[6].rjust(line_width/5)

# Example 9.4

# def ask(question)
#   while true
#     puts question
#     reply = gets.chomp.downcase
#
#     if (reply == 'yes' || reply == 'no')
#       if reply == 'yes'
#         return true
#       else
#         return false
#       end
#     else
#       puts 'Please answer "yes" or "no".'
#     end
#   end
# end
#
# ask("Do you like eating tacos?")
# bed_wet = ask("Do you wet the bed?")
# puts bed_wet

# def old_roman_numerals(num)
#   roman = ''
#
#   roman << 'M' * (num / 1000)
#   roman << 'D' * (num % 1000 / 500)
#   roman << 'C' * (num % 500 / 100)
#   roman << 'L' * (num % 100 / 50)
#   roman << 'X' * (num % 50 / 10)
#   roman << 'V' * (num % 10 / 5)
#   roman << 'I' * (num % 5 / 1)
# end
#
# puts old_roman_numerals(2699)

# def modern_roman_numerals(num)
#   thousands = (num / 1000)
#   hundreds = (num % 1000 / 100)
#   tens = (num % 100 / 10)
#   ones = (num % 10)
#
#   roman = 'M' * thousands
#
#   if hundreds == 9
#     roman = roman + 'CM'
#   elsif hundreds == 4
#     roman = roman + 'CD'
#   else
#     roman = roman + 'D' * (num % 1000 / 500)
#     roman = roman + 'C' * (num % 500 / 100)
#   end
#
#   if tens == 9
#     roman = roman + 'XC'
#   elsif tens == 4
#     roman = roman + 'XL'
#   else
#     roman = roman + 'L' * (num % 100 / 50)
#     roman = roman + 'X' * (num % 50 / 10)
#   end
#
#   if ones == 9
#     roman = roman + 'IX'
#   elsif ones == 4
#     roman = roman + 'IV'
#   else
#     roman = roman + 'V' * (num % 10 / 5)
#     roman = roman + 'I' * (num % 5 / 1)
#   end
#
# end
#
# puts modern_roman_numerals(1999)

# Example 10.1

# M = 'land'
# o = 'water'
#
# world = [[o,o,o,o,M,o,o,o,o,o,o],
#          [o,o,o,o,M,M,o,o,o,o,o],
#          [o,o,o,M,o,o,o,o,M,M,o],
#          [o,o,o,M,o,o,o,o,o,M,o],
#          [o,o,o,M,o,M,M,o,o,o,o],
#          [o,o,o,o,M,M,M,M,o,o,o],
#          [M,M,M,M,M,M,M,M,M,M,M],
#          [o,o,o,M,M,o,M,M,M,o,o],
#          [o,o,o,o,o,o,M,M,o,o,o],
#          [o,M,o,o,o,M,o,o,o,o,o],
#          [o,o,o,o,o,M,o,o,o,o,o]]
#
# def continent_size(world, x, y)
#
#   if y + 1 > world.length || x + 1 > world.length
#     return 0
#   # elsif y - 1 > world.length || x - 1 > world.length
#   #   return 0
#   end
#
#   if world[y][x] != 'land'
#     return 0
#   end
#
#   size = 1
#   world[y][x] = 'counted land'
#
#   size = size + continent_size(world, x-1, y-1)
#   size = size + continent_size(world, x, y-1)
#   size = size + continent_size(world, x+1, y-1)
#   size = size + continent_size(world, x-1, y)
#   size = size + continent_size(world, x+1, y)
#   size = size + continent_size(world, x-1, y+1)
#   size = size + continent_size(world, x, y+1)
#   size = size + continent_size(world, x+1, y+1)
#
#   size
#
# end
#
# puts continent_size(world, 5, 5)


# def sort_array(array_to_sort)
#
#   sorted_array = []
#
#   until array_to_sort.empty?
#     array_to_sort.each do |i|
#       if i == array_to_sort.min
#         sorted_array.push(i)
#         array_to_sort.delete(i)
#       end
#     end
#   end
# # puts array_to_sort
# sorted_array
# end

# def sort(to_sort)
#   return to_sort if to_sort.length <= 1
#
#   middle = to_sort.pop
#   less = to_sort.select{|x| x < middle}
#   more = to_sort.select{|x| x >= middle}
#
#   sort(less) + [middle] + sort(more)
# end

# to_sort = ['Marty', 'John', 'Trevor', 'Danielle', 'Chris', 'Paul', 'Luke', 'JM', 'Marie']
# to_sort = [1, 2, 5, 3, 78, 45, 4, 8, 2]
# to_sort = ['Marty', 'John', 'Trevor', 'Danielle', 'Chris', 'Paul', 'Luke', 'JM', 'Marie', 'Marty', 'John']
# to_sort = ['Marty', 'Marty']
# puts sort(to_sort)

# def shuffle(arr)
#   arr.shuffle
# end
#
# puts(shuffle(['Marty', 'John', 'Trevor', 'Danielle', 'Chris', 'Paul', 'Luke', 'JM', 'Marie', 'Marty', 'John']))

# def dictionary_sort(to_sort)
#   return to_sort if to_sort.length <= 1
#
#   middle = to_sort.pop
#   less = to_sort.select{|x| x.downcase < middle.downcase}
#   more = to_sort.select{|x| x.downcase >= middle.downcase}
#
#   dictionary_sort(less) + [middle] + dictionary_sort(more)
# end
#
# puts dictionary_sort(['Marty', 'John', 'Trevor', 'Danielle', 'Chris', 'paul', 'Luke', 'JM', 'marie', 'Marty', 'john'])

#  Example 11.3

# filename = 'new_file.txt'
# content = 'Here is some content'
#
# File.open filename, 'w' do |f|
#   f.write content
# end
#
# read_file = File.read filename
#
# puts (read_file == content)

# filename = 'new_file_2.txt'
# content = 'Here is some content for my file for testing purposes'
#
# File.open filename, 'w' do |f|
#   f.write content
# end
#
# puts File.read filename

# require 'yaml'
#
# content_array = ['some day',
#                  'over the rainbow',
#                  'way up high']
#
# content = content_array.to_yaml
#
# filename = 'song.txt'
#
# File.open filename, 'w' do |f|
#   f.write content
# end
#
# read_file = File.read filename
#
# puts read_file
