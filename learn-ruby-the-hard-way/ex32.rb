the_count = [1, 2, 3, 4, 5]
fruits = ['apples', 'oranges', 'pears', 'apricots']
change = [1, 'pennies', 2, 'dimes', 3 , 'quarters']

for number in the_count
  puts "This is count #{number}"
end

the_count.each {|i| puts "This is count #{i}"}

fruits.each do |fruit|
  puts "A fruit of type: #{fruit}"
end

elements = []

elements.each {|i| puts "The elements are #{i}"}

(0..5).each do |i|
  puts "adding #{i} to the list"
  elements.push(i)
end

(0...5).each do |i|
  puts "adding #{i} to the list"
  elements.push(i)
end