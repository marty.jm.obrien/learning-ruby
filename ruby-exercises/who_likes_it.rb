def likes(names)
  if names.length === 1
    puts names[0] + " likes this"
  elsif names.length === 2
    puts names.join(' and') + " likes this"
  elsif names.length === 3
      puts names[0] + ", " + names[1] + " and " + names[2] + " likes this"
  elsif names.length >= 4
    puts names[0..1].join(', ') + " and #{names[2..-1].length} others like this"
  else
    puts "no one likes this"
  end
end