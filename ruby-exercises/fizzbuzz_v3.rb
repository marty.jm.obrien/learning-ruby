# puts "Welcome to FizzBuzz, please enter one or more numbers: "
#
# user_input = gets.chomp
# num_arr = user_input.split(" ")
#
# for i in num_arr
#   x = i.to_i
#     if x%3 == 0 && x%5 == 0
#       puts "FizzBuzz"
#     elsif x%3 == 0
#       puts "Fizz"
#     elsif x%5 == 0
#       puts "Buzz"
#     else
#       puts "Sorry, please try again."
#     end
# end

def fizzBuzz(*user_input)
  num_arr = user_input

  for i in num_arr
    x = i.to_i
      if x%3 == 0 && x%5 == 0
        puts "FizzBuzz"
      elsif x%3 == 0
        puts "Fizz"
      elsif x%5 == 0
        puts "Buzz"
      else
        puts "Sorry, please try again."
      end
  end
end
