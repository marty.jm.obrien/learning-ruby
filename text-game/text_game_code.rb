def code (num) 
  puts "Well done. You have found an escape code digit! The digit is #{num}."
  padlock_code = num 
  return padlock_code
end
 
def end_room
   puts "Well done, you have made it to the final challenge!"
end
 
def entrance_again
   puts "You find yourself back at the entrance, in front of the 4 doors once again."
end

digit_one = code("3")
digit_two = code("5")
digit_three = code("7")
digit_four = code("1")

padlock_code = []

padlock_code.push(digit_one)
padlock_code.push(digit_two)
padlock_code.push(digit_three)
padlock_code.push(digit_four)

if padlock_code.length == 4
  end_room
else
  entrance_again
end