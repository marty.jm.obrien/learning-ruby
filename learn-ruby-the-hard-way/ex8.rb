x = 100

y = """
there are #{x} people in my house!
"""

a =  '''
there are #{x} people in my house!
'''

puts y

puts a

puts "every day\nI wake up\ntired"

puts "Should I stay?\\Should I go?"

puts "\tEveryone just \"loves\" me!"

puts "\rEveryone just \"loves\" me!"

marty = """
Marty is:
\t1 - Amazing
\t2 - Great
\t3 - Super
"""

puts marty

months = "\nJan\nFeb\nMar\nApr"

puts "Here is a list of the months: #{months}"
