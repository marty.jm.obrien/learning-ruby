class Book
    def title
        @title
    end
    def title=(new_title)
        @title = new_title
        capitalize(@title)
    end
    def capitalize(book_title)
        not_to_capitalize = ["a", "and", "an", "to", "in", "the", "but", "or", "for", "on", "as", "nor", "at", "by", "in", "of", "up"]
        if book_title.include?(' ')
            title = book_title.split ' '
            title.each do |i|
                if i.length >= 4 || i == "i" || i == "was"
                i.capitalize!
                end
            end
            title[1].capitalize! unless not_to_capitalize.include?(title[1])
            title.first.capitalize!
            title.last.capitalize!
            @title = title.join ' '
        else
            @title = book_title.capitalize
        end
    end
end
