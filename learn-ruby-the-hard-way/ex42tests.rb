class HumanDetails
  
  attr_accessor :age, :children, :medical

    def initialize(name)
      @name = name
      @age = age
      @children = []
      @medical = {}
    end
  
  def HumanDescription
    if @age >= 18
      puts "Welcome #{@name}"
      children.each {|child| puts "Your children are named #{child}"}
      medical.each do {|a, b| puts "Your #{a} is #{b}"}
    else
      puts "Sorry, you are not old enough to access this data"
    end
  end
end
    