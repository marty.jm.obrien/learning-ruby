room_1 = <<END
You walk down the stairs and enter a dark basement.
There is a single small light bulb hanging from a wooden beam.
It slowly swings in the still dank air.
You look around and can barely make out three doors on a far wall.
You walk slowly and carefully towards the doors as you decide which to walk through.
END

door_2_story = <<END
As you turn the handle the door suddenly slams open of its own accord.
You stumble back into the dark and knock into a work bench.
There are rusty old tools on the bench and you grab one for protection.
Which one do you choose?
END

hammer_story = <<END
You pick up the hammer and walk slowly through the door, into the darkness.
As you go further into the darkness you hear the door slam and lock behind you.
With no way of knowing what is with you in the darkness you panic and run.
After only a few yards your foot gets caught and you fall to the floor with a crack, losing your hammer.
As you grab for the hammer your hand suddenly makes contact with skin and cloth.
Pulling your hand away you hear a childs laughter, just before your world turns black.
YOU DIED.
END

screwdriver_story = <<END
You grab for the screwdriver but you fumble and it drops to the floor.
As you kneel down and search for it you feel a warm breath of the back of your neck.
You turn to see the glint of steel before your world turns black.
YOU DIED."
END

saw_story = <<END
You grab the saw by the handle and pick it up with an audible \'shingggg\' as it scrapes the bench.
You run through the pitch black doorway knowing you have nothing left to live for.
Suddenly the floor falls away and you find yourself falling, down, down, further into darkness.
You hit the floor at terminal velocity and your body jumps.
As your eyes open and dart around the room you realise you are at home, in bed.
It was all just a dream.
END

garden_story = <<END
Your pulse quickens as you slowly turn the handle and push the door open.
As it creaks and groans you are suddenly met by blinding sunshine, birdsong and laughing.
You realise that you are in your back garden and have forgotten why you went into the basement.
YOU ARE AN IDIOT.
END

puts "-----------------"
puts room_1
puts "-----------------"

door = 0

until [1, 2, 3].include? door do
  puts "Which door have you choosen? 1, 2 or 3?"
  print "> "
  door = $stdin.gets.chomp.to_i
end

if door == 1
  puts "You turn the handle and push, only to find that the door is locked."

elsif door == 2
  puts "-----------------"
  puts door_2_story
  puts "-----------------"
  
  tool = 0
  
  until ["hammer", "screwdriver", "saw"].include? tool do
    puts "hammer, " + "screwdriver or " + "saw\n"
    print "> "
    tool = $stdin.gets.chomp
  end
  
    if tool == "hammer"
      puts "-----------------"
      puts hammer_story
      puts "-----------------"
    elsif tool == "screwdriver"
      puts "-----------------"
      puts screwdriver_story
      puts "-----------------"
    elsif tool == "saw"
      puts "-----------------"
      puts saw_story
      puts "-----------------"
    else
      puts "You grab for #{tool} but there is nothing there."
      puts "As you struggle to find it you hear footsteps behind you."
      puts "You try to run but a hand grabs your collar and your world turns black."
      puts "YOU DIED"
    end
      
elsif door == 3
  puts "-----------------"
  puts garden_story
  puts "-----------------"
  
else
  puts "-----------------"
  puts "You turn away from the doors and sit on the floor crying, waiting to die."
  puts "-----------------"
  
end 