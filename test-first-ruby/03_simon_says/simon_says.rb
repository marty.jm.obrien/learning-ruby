def echo(original)
    original
end

def shout(original)
    original.upcase
end

def repeat(original, repeats=2)
    ([original] * repeats).join ' '
end

def start_of_word(word, letters)
    new_string = []
    (0...letters).each do |i|
        new_string << word[i]
    end
    new_string.join ''
end

def first_word(sentence)
    (sentence.split ' ').shift
end

def titleize(words)
    if words.include?(' ')
        title = (words.split ' ').each do |i|
            i.capitalize! unless i.length <= 4
        end
        title.first.capitalize!
        title.last.capitalize!
        title.join ' '
    else
        words.capitalize
    end
end