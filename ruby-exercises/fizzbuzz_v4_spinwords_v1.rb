# puts "Welcome to FizzBuzz, please enter one or more numbers: "
#
# user_input = gets.chomp
# num_arr = user_input.split(" ")
#
# for i in num_arr
#   x = i.to_i
#     if x%3 == 0 && x%5 == 0
#       puts "FizzBuzz"
#     elsif x%3 == 0
#       puts "Fizz"
#     elsif x%5 == 0
#       puts "Buzz"
#     else
#       puts "Sorry, please try again."
#     end
# end

def fizzBuzz(*user_input)
  num_arr = user_input

  # for i in num_arr
  num_arr.each do |i|
    x = i.to_i
      if x%3 == 0 && x%5 == 0
        puts "FizzBuzz"
      elsif x%3 == 0
        puts "Fizz"
      elsif x%5 == 0
        puts "Buzz"
      else
        puts "Sorry, please try again."
      end
  end
end

fizzBuzz(4, 8, 15, 3, 5)

# def spinWords(string)
#   input_string = string.split(" ")
#   output_string = []
#
#   input_string.each do |i|
#     if i.length >= 5
#       a = i.reverse
#       output_string << a
#     else
#       output_string << i
#     end
#   end
#
#   puts output_string.join(" ")
# end
#
# spinWords("hey fellow warriors")
