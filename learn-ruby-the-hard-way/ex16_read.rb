filename = ARGV.first

puts "Would you like to read #{filename}?"
puts "If you don't want to hit CRTL-C (^C)"
puts "If you would like to then hit return"

$stdin.gets

puts "Opening the file for you now..."
txt = open(filename, 'r')

puts "Here is your file:"
puts txt.read
txt.close

puts "Would you like to delete and replace the contents of this file?"
puts "If no then please hit CRTL-C (^C)"
puts "If yes then please hit return"

$stdin.gets

puts "Your file will now be cleared of all contents"
new_txt = open(filename, 'w')

puts "Please enter the new text you would like:"
new_content = $stdin.gets.chomp

puts "Your new text is being added to your file"

new_txt.write(new_content)

puts "Your file has been updated"
new_txt.close
