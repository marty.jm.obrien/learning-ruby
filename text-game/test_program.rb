class Person
  attr_accessor :name, :family, :height_weight
  
  def initialize
    @name = name
    @family = []
    @height_weight = {}
  end
  
  def user_login
    puts "Please enter your password to access this information."
    print "> "
    password_attempt = gets.chomp.to_i
      
      until password_attempt == password
        puts "Sorry, that password in incorrect. Please try again."
        print "> "
        password_attempt = gets.chomp.to_i
      end
    
      if password_attempt == password
        puts "Thank you. Please enter the name of the person you wish to view the data of."
        print "> "
        person_name = gets.chomp
        @name = person_name
      else
        exit
      end
  end
  
  def description
    puts "You are now viewing information regarding #{@name}."
    @family = ["Danielle", "Florence", "Fred"]
    puts "#{@name} has the following family members:"
    family.each {|i| puts i}
    @height_weight = {'height' => '5ft 8in', 'weight' => '10st'}
    puts "#{@name} is #{height_weight['height']} tall and weighs #{height_weight['weight']}."
  end
  
  private
  
  def password
    1234
  end
end

new_person = Person.new
new_person.user_login
new_person.description







